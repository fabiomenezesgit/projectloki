package br.com.wiretecnologia.config;

import br.com.wiretecnologia.JsonUtils;
import br.com.wiretecnologia.entities.Pedido;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


@Service
public class NfeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NfeController.class);

    @Value("${api-nfe.url}")
    private String URL_API;

    @Autowired
    JsonUtils jsonUtils;

    public void getEnviaNfe() throws IOException {
        URL url = new URL(URL_API);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream content = connection.getInputStream();
    }

    private boolean verificaConexão(HttpURLConnection conn) throws IOException {
        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        return true;
    }

    public void enviaNfetoApi(Pedido pedido) {
        try {
            URL url = new URL(URL_API);
            LOGGER.info("ENVIANDO DADOS PARA API NFE: {}", url);
            HttpURLConnection conn =  (HttpURLConnection) url.openConnection();
            LOGGER.info("VERIFICANDO STATUS DA CONEXÃO COM API: {}", verificaConexão(conn));
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(jsonUtils.convertObjectToJson(pedido).getBytes("UTF-8"));
            os.close();
        } catch (IOException e) {
            LOGGER.error("# Erro ->",e);
        }
    }
}
