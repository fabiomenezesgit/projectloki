package br.com.wiretecnologia.service;

import br.com.wiretecnologia.entities.ServicoAgenda;
import br.com.wiretecnologia.repositories.ServicoAgendaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ServicoAgendaRpService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServicoAgendaRpService.class);
    @Autowired
    ServicoAgendaRepository servicoAgendaRepository;

    public ServicoAgenda save(ServicoAgenda servicoAgenda){
        return servicoAgendaRepository.saveAndFlush(servicoAgenda);
    }


    public Set<ServicoAgenda> getServicos(String user){
        return servicoAgendaRepository.findByUser(user);
    }
}
