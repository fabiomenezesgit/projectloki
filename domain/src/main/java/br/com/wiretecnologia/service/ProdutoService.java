package br.com.wiretecnologia.service;

import br.com.wiretecnologia.entities.Produto;
import br.com.wiretecnologia.repositories.ProdutoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Set;


@Service
public class ProdutoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProdutoService.class);

    @Autowired
    private ProdutoRepository produtoRepository;


    public Set<Produto> findProdutosName(@PathVariable String name, String user) {
        Set<Produto> produtos = produtoRepository.findByNomeContainingIgnoreCaseAndUserEquals(name, user);
        produtos.forEach(
                produto
                        ->
                        produto.setQuantidade(1)
        );
        return produtos;
    }

    public Produto insertProduto(Produto obj) {
        return produtoRepository.save(obj);
    }

    public ResponseEntity<?> findAllProdutos(String user) {
        return new ResponseEntity<>(produtoRepository.findAllByUser(user), HttpStatus.OK);
    }

    public ResponseEntity<?> findMinEstoque(String user) {
        try {
            return new ResponseEntity<>(produtoRepository.verificaMinEstoque(user), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.info("ERRO",e);
            return ResponseEntity.notFound().build();
        }
    }

}
