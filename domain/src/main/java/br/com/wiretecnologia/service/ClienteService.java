package br.com.wiretecnologia.service;

import br.com.wiretecnologia.entities.Cliente;
import br.com.wiretecnologia.repositories.ClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ClienteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteService.class);

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criar(Cliente cliente) {
        LOGGER.info("SALVANDO CLIENTE: {}", cliente.getNome());
        return clienteRepository.save(cliente);
    }

    public Cliente atualizar(Cliente cliente) {
        LOGGER.info("#Atualizando cliente: {}", cliente.getId());
        Cliente clienteSalvo= null ;
        return clienteRepository.save(clienteSalvo);
    }

    public Cliente deletarCliente(Cliente cliente) {
        LOGGER.info("#Deletandio cliente: {}", cliente.getId());
        Cliente clienteSalvo = null;
        clienteRepository.delete(clienteSalvo);
        return clienteRepository.save(clienteSalvo);
    }

    public Set<Cliente> getNomeCliente(String name, String user) {
        return clienteRepository.findByNomeContainingIgnoreCaseAndUserEquals(name, user);
    }

}