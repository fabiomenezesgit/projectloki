package br.com.wiretecnologia.repositories;

import br.com.wiretecnologia.entities.ServicoAgenda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ServicoAgendaRepository extends JpaRepository<ServicoAgenda,Long> {

    Set<ServicoAgenda> findByUser(String user);
}
