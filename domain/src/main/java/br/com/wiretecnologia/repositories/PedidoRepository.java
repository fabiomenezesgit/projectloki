package br.com.wiretecnologia.repositories;

import br.com.wiretecnologia.entities.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;


@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {

    Set<Pedido> findById(int id);
}
