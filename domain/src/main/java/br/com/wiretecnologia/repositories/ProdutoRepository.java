package br.com.wiretecnologia.repositories;

import br.com.wiretecnologia.entities.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

    Set<Produto> findByNomeContainingIgnoreCaseAndUserEquals(String name, String user);

    List<Produto> findAllByUser(String user);

    @Query(value = "SELECT * FROM produtos WHERE quantidade <= qtd_min_estoque AND username=?", nativeQuery = true)
    Set<Produto> verificaMinEstoque(String user);

//    @Query(value = "SELECT TOP 5 FROM produtos order by pedido desc", nativeQuery = true)
//    List<Produto> verificaMaxVendas();

    Produto findById(int id);

}
