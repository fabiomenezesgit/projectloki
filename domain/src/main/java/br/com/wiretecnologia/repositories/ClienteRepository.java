package br.com.wiretecnologia.repositories;

import br.com.wiretecnologia.entities.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	

	Set<Cliente> findByNomeContainingIgnoreCaseAndUserEquals(String name, String user);
	
}
