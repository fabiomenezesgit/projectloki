package br.com.wiretecnologia.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "PEDIDO")
public class Pedido implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(name = "FORMA_PAGAMENTO")
    private String formaPagamento;

    @Column(name = "DATA_PEDIDO")
    private String dataPedido;

    @Column(name = "DESCONTO")
    private int desconto;

    @Column(name = "VALOR_PARCELA")
    private float valorParcela;

    @Column(name = "NUMERO_PARCELA")
    private float numParcela;

    @ManyToMany
    @JoinTable(name="PEDIDO_PRODUTO",
            joinColumns={@JoinColumn(name="PRODUTO_ID")},
            inverseJoinColumns={@JoinColumn(name="PEDIDO_ID")})
    private List<Produto> listaProduto;

    @ManyToOne
    private Cliente cliente;

    @OneToOne
    private ServicoAgenda servicoAgenda;

    @Column(name = "USERNAME", nullable = false)
    private String user;

    @Column(name = "VALOR_TOTAL")
    private float valorTotal;

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public float getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(float valorParcela) {
        this.valorParcela = valorParcela;
    }

    public float getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(float numParcela) {
        this.numParcela = numParcela;
    }

    public List<Produto> getListaProduto() {
        return this.listaProduto;
    }

    public void setListaProduto(List<Produto> listaProduto) {
        this.listaProduto = listaProduto;
    }

    public int getId() {
        return id;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public String getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
