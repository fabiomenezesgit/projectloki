package br.com.wiretecnologia.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Table(name = "PRODUTOS")
@Entity
public class Produto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private int id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "DISPONIBILIDADE")
    private boolean disponibilidade;

    @Column(name = "PRECO")
    private String preco;

    @Column(name = "QUANTIDADE")
    private int quantidade;

    @Column(name = "TX_GANHO")
    private int txGanho;

    @Column(name = "FORNECEDOR_ID")
    private int fornecedorId;

    @Column(name = "VALOR_COMPRA")
    private float valorCompra;

    @Column(name = "VALOR_GANHO")
    private float valorGanho;

    @Column(name = "QTD_MIN_ESTOQUE")
    private int quantidadeMinEstoque;

    @Column(name = "COD_BARRA")
    private Long codBarra;

    @Column(name = "USERNAME", nullable = false)
    private String user;

    @JsonIgnore
    @ManyToMany(mappedBy="listaProduto")
    private List<Pedido> pedido;

    public int getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public float getValorGanho() {
        return valorGanho;
    }

    public void setValorGanho(float valorGanho) {
        this.valorGanho = valorGanho;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(boolean disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getTxGanho() {
        return txGanho;
    }

    public void setTxGanho(int txGanho) {
        this.txGanho = txGanho;
    }

    public int getFornecedorId() {
        return fornecedorId;
    }

    public void setFornecedorId(int fornecedorId) {
        this.fornecedorId = fornecedorId;
    }

    public float getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(float valorCompra) {
        this.valorCompra = valorCompra;
    }

    public int getQuantidadeMinEstoque() {
        return quantidadeMinEstoque;
    }

    public void setQuantidadeMinEstoque(int quantidadeMinEstoque) {
        this.quantidadeMinEstoque = quantidadeMinEstoque;
    }

    public Long getCodBarra() {
        return codBarra;
    }

    public void setCodBarra(Long codBarra) {
        this.codBarra = codBarra;
    }

    public List<Pedido> getPedido() {
        return pedido;
    }

    public void setPedido(List<Pedido> pedido) {
        this.pedido = pedido;
    }

}
