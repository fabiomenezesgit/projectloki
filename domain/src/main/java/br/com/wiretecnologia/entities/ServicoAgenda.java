package br.com.wiretecnologia.entities;

import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "SERVICO_AGENDA")
public class ServicoAgenda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "OS_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "START_EXECUCAO")
    private LocalDateTime startDate;

    @Column(name = "END_EXECUCAO")
    private LocalDateTime endDate;

    @Column(name = "TITULO")
    private String title;

    @Column(name = "COLOR_EVENT")
    private String colorEvent;

    @UpdateTimestamp
    @Column(name = "DT_EXECUCAO")
    private  LocalDateTime dtExecucao;

    @Column(name = "MENSAGEM",columnDefinition = "TEXT")
    private String message;


    @ManyToOne(fetch = FetchType.EAGER)
    private Cliente clienteId;

    @OneToOne(mappedBy = "servicoAgenda")
    private Pedido pedidoId;

    @ManyToOne(fetch = FetchType.EAGER)
    private Operacional operacional;

    @Column(name = "USERNAME", nullable = false)
    private String user;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }


    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColorEvent() {
        return colorEvent;
    }

    public void setColorEvent(String colorEvent) {
        this.colorEvent = colorEvent;
    }

    public LocalDateTime getDtExecucao() {
        return dtExecucao;
    }

    public void setDtExecucao(LocalDateTime dtExecucao) {
        this.dtExecucao = dtExecucao;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Cliente getClienteId() {
        return clienteId;
    }

    public void setClienteId(Cliente clienteId) {
        this.clienteId = clienteId;
    }

    public Pedido getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Pedido pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Operacional getOperacional() {
        return operacional;
    }

    public void setOperacional(Operacional operacional) {
        this.operacional = operacional;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServicoAgenda that = (ServicoAgenda) o;
        return id == that.id &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(title, that.title) &&
                Objects.equals(colorEvent, that.colorEvent) &&
                Objects.equals(dtExecucao, that.dtExecucao) &&
                Objects.equals(message, that.message) &&
                Objects.equals(clienteId, that.clienteId) &&
                Objects.equals(pedidoId, that.pedidoId) &&
                Objects.equals(operacional, that.operacional) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startDate, endDate, title, colorEvent, dtExecucao, message, clienteId, pedidoId, operacional, user);
    }
}
