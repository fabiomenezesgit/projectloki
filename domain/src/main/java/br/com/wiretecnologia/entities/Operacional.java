package br.com.wiretecnologia.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "OPERACIONAL")
public class Operacional implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CPF")
    private int cpf;

    @Column(name = "SETOR")
    private String setor;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ServicoAgenda> servicoAgenda;


    @Column(name = "USERNAME", nullable = false)
    private String user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCpf() {
        return cpf;
    }

    public void setCpf(int cpf) {
        this.cpf = cpf;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public List<ServicoAgenda> getServicoAgenda() {
        return servicoAgenda;
    }

    public void setServicoAgenda(List<ServicoAgenda> servicoAgenda) {
        this.servicoAgenda = servicoAgenda;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
