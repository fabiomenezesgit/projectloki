package br.com.wiretecnologia;


import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class RequestUtils {

    public String extractUser(HttpServletRequest httpRequest){
        String token = httpRequest.getHeader("Authorization");
        return (Jwts.parser().setSigningKey("MySecret").parseClaimsJws(token.replace("Bearer", "")).getBody().getSubject());
    }
}
