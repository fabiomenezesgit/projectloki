FROM openjdk:8-jre

ADD loki/service/target/application-0.0.1-SNAPSHOT.jar /
RUN  cd /loki/service/target/ && ls
CMD ["java", "-jar", "loki/service/target/application-0.0.1-SNAPSHOT.jar"]