package br.com.wiretecnologia.controller;


import br.com.wiretecnologia.RequestUtils;
import br.com.wiretecnologia.entities.Produto;
import br.com.wiretecnologia.service.ProdutoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Objects;
import java.util.Set;

@Controller
@RequestMapping(value = "/produto")
public class ProdutoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProdutoController.class);

    private ProdutoService produtoService;
    private RequestUtils requestUtils;

    public ProdutoController(ProdutoService produtoService, RequestUtils requestUtils) {
        this.produtoService = produtoService;
        this.requestUtils = requestUtils;
    }

    @GetMapping()
    public @ResponseStatus
    ResponseEntity getnome(@Valid String nome, HttpServletRequest request) {
        String user = requestUtils.extractUser(request);
        LOGGER.info("BUSCASCANDO POR NOME\n");
        Set<Produto> produtoSet = produtoService.findProdutosName(nome, user);

        if (Objects.nonNull(produtoSet)) {
            return ResponseEntity.ok().body(produtoSet);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping()
    public @ResponseBody
    ResponseEntity insere(@Valid @RequestBody Produto produto, HttpServletRequest request) {
        String user = requestUtils.extractUser(request);
        produto.setUser(user);
        LOGGER.info("INSERINDO SUCESSO\n");

            produtoService.insertProduto(produto);
            return ResponseEntity.status(200).body("PRODUTO CADASTRADO");
    }

    @GetMapping("/getAll")
    public @ResponseStatus
    ResponseEntity getAll(HttpServletRequest request) {
        String user = requestUtils.extractUser(request);
        LOGGER.info("BUSCASCANDO TODOS PRODUTOS \n");
        LOGGER.info("USER IS : {}", user);
        return produtoService.findAllProdutos(user);
    }

    @GetMapping("/getEstoque")
    public @ResponseStatus
    ResponseEntity getEstoque(HttpServletRequest request) {
        LOGGER.info("BUSCA QUANTIDADE MININA DE ESTOQUE \n");
        String user = requestUtils.extractUser(request);
        return produtoService.findMinEstoque(user);
    }
}
