package br.com.wiretecnologia.controller;

import br.com.wiretecnologia.RequestUtils;
import br.com.wiretecnologia.entities.ServicoAgenda;
import br.com.wiretecnologia.service.ServicoAgendaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Objects;
import java.util.Set;

@Controller
@RequestMapping("/ServicoAgenda")
public class ServicoAgendaController {


    private static final Logger LOGGER = LoggerFactory.getLogger(br.com.wiretecnologia.controller.ClientesController.class);

    private final RequestUtils requestUtils;
    private final ServicoAgendaService servicoAgendaService;

    @Autowired
    public ServicoAgendaController(RequestUtils requestUtils, ServicoAgendaService servicoAgendaService) {
        this.requestUtils = requestUtils;
        this.servicoAgendaService = servicoAgendaService;
    }

    @GetMapping()
    public @ResponseStatus
    ResponseEntity getAgendamentos(HttpServletRequest request) {
        String user = requestUtils.extractUser(request);
        LOGGER.info("BUSCANDO AGENDAMENTOS MENSAL\n");
        Set<ServicoAgenda> servicoAgendaSet = servicoAgendaService.getServicos(user);

        if (Objects.nonNull(servicoAgendaSet)) {
            return ResponseEntity.ok().body(servicoAgendaSet);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PutMapping()
    public @ResponseBody
    ResponseEntity editar(int id, HttpServletRequest request) {
        String user = requestUtils.extractUser(request);
        return ResponseEntity.ok("OK");
    }

    @PostMapping()
    public @ResponseBody
    ResponseEntity criar(@RequestBody ServicoAgenda servicoAgenda, HttpServletRequest request) {
        String user = requestUtils.extractUser(request);
        LOGGER.info("# Salvando servico: {}\n", servicoAgenda.getUser());
        servicoAgenda.setUser(user);

        if (Objects.nonNull(servicoAgenda)) {
            servicoAgendaService.insertServico(servicoAgenda);
            return ResponseEntity.ok().body(servicoAgenda);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }


    @DeleteMapping
    public ResponseEntity listar(@RequestBody @Valid ServicoAgenda servicoAgenda,
                                 HttpServletRequest request) {
        servicoAgenda.setUser( requestUtils.extractUser(request));
        LOGGER.info("# Serviço {}",servicoAgenda.getId());
        return ResponseEntity.ok().body("# EXCLUIDO COM SUCESSO");
    }
}

