package br.com.wiretecnologia.controller;


import br.com.wiretecnologia.RequestUtils;
import br.com.wiretecnologia.config.NfeController;
import br.com.wiretecnologia.entities.Pedido;
import br.com.wiretecnologia.service.PedidoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping(value = "/pedido")
public class PedidoController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PedidoController.class);

    private PedidoService pedidoService;
    private NfeController nfeController;
    private RequestUtils requestUtils;

    @Autowired
    public PedidoController(PedidoService pedidoService, NfeController nfeController, RequestUtils requestUtils) {
        this.pedidoService = pedidoService;
        this.nfeController = nfeController;
        this.requestUtils = requestUtils;
    }

    @PostMapping()
    public @ResponseBody
    ResponseEntity savePedido(@Valid @RequestBody Pedido pedido, HttpServletRequest request) {
        pedido.setUser(requestUtils.extractUser(request));
        LOGGER.info("SALVANDO PEDIDO");
        pedidoService.insertPedido(pedido);
        LOGGER.info("PEDIDO SALVO ID:{}", pedido.getId());
        LOGGER.info("GERANDO NFE DO PEDIDO ID:{}", pedido.getId());
        nfeController.enviaNfetoApi(pedido);
        return ResponseEntity.ok().body("PEDIDO CADASTRADO");
    }

    @GetMapping
    public @ResponseBody
    ResponseEntity getPedido(@Valid int obj) {
        LOGGER.info("BUSCANDO PEDIDO");
        pedidoService.findProdutosName(obj);
        return ResponseEntity.ok().body("PEDIDO BUSCADO COM SUCESSO");
    }

    @PutMapping
    public @ResponseBody
    ResponseEntity editPedido(@Valid Pedido pedido) {
        LOGGER.info("EDITANDO PEDIDO");
        pedidoService.insertPedido(pedido);
        return ResponseEntity.ok().body("PEDIDO EDITADO COM SUCESSO");
    }

}
