package br.com.wiretecnologia.controller;

import br.com.wiretecnologia.RequestUtils;
import br.com.wiretecnologia.entities.Cliente;
import br.com.wiretecnologia.service.ClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Set;


@Controller
@RequestMapping("/cliente")
public class ClientesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientesController.class);

    private final RequestUtils requestUtils;
    private final ClienteService clienteService;

    public ClientesController(RequestUtils requestUtils, ClienteService clienteService) {
        this.requestUtils = requestUtils;
        this.clienteService = clienteService;
    }

    @GetMapping()
    public @ResponseStatus
    ResponseEntity getnome(String nome, HttpServletRequest request) {
        String user = requestUtils.extractUser(request);
        LOGGER.info("BUSCANDO CLIENTE POR NOME\n");
        Set<Cliente> produtoSet = clienteService.getNomeCliente(nome, user);

        if (Objects.nonNull(produtoSet)) {
            return ResponseEntity.ok().body(produtoSet);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PutMapping()
    public @ResponseBody
    ResponseEntity editar(Cliente id) {
        return ResponseEntity.ok("OK");
    }

    @PostMapping()
    public @ResponseBody
    ResponseEntity criar(@RequestBody Cliente cliente, HttpServletRequest request) {
        String user = requestUtils.extractUser(request);
        LOGGER.info("SALVANDO CLIENTE: {}\n",cliente.getNome());
        cliente.setUser(user);
        clienteService.criar(cliente);
        if (Objects.nonNull(cliente)) {
            return ResponseEntity.ok().body(cliente);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping
    public ResponseEntity listar(@RequestBody Cliente cliente) {
        return ResponseEntity.ok().body("EXCLUIDO COM SUCESSO");
    }
}