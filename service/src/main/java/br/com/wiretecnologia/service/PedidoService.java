package br.com.wiretecnologia.service;

import br.com.wiretecnologia.DateUtils;
import br.com.wiretecnologia.entities.Pedido;
import br.com.wiretecnologia.entities.Produto;
import br.com.wiretecnologia.repositories.PedidoRepository;
import br.com.wiretecnologia.repositories.ProdutoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PedidoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PedidoService.class);
    @Autowired
    PedidoRepository pedidoRepository;

    @Autowired
    ProdutoRepository produtoRepository;

    @Autowired
    DateUtils dateUtils;

    public Pedido insertPedido(Pedido obj) {
            obj.setDataPedido(dateUtils.getDateTime());
            pedidoRepository.save(obj);
            atualizaProduto(obj);
        return obj;
    }


    public Set<Pedido> findProdutosName(int id) {
       return  pedidoRepository.findById(id);

    }


    private void atualizaProduto(Pedido pedido) {
        LOGGER.info("ATUALIZANDO DATABASE");
        pedido.getListaProduto().forEach(produto -> {
            int param = produto.getId();
            int quant = produto.getQuantidade();
            Produto prod = produtoRepository.findById(param);
            prod.setQuantidade(prod.getQuantidade() - quant);
            produtoRepository.saveAndFlush(prod);
        });

    }


}
