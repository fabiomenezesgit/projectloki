package br.com.wiretecnologia.service;

import br.com.wiretecnologia.entities.ServicoAgenda;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ServicoAgendaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServicoAgendaService.class);


    @Autowired
    ServicoAgendaRpService servicoAgendaRpService;


    public ServicoAgenda insertServico(ServicoAgenda servicoAgenda) {
        return servicoAgendaRpService.save(servicoAgenda);
    }

    public Set<ServicoAgenda> getServicos(String user) {
        return  servicoAgendaRpService.getServicos(user);

    }




}
